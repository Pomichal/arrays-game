﻿using UnityEngine;

public class PortalBehaviour : MonoBehaviour
{
    public Material activeMat;
    public Material baseMat;
    public InGamePopup inGamePopup;
    public int portalType;

    void Start()
    {
        baseMat = gameObject.GetComponent<MeshRenderer>().material;
        inGamePopup = FindObjectOfType<InGamePopup>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<GrimReaperBehaviour>().activePortal = portalType;
            gameObject.GetComponent<MeshRenderer>().material = activeMat;
            if(!inGamePopup.inventoryShown)
            {
                inGamePopup.ShowInventory();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<GrimReaperBehaviour>().activePortal = -1;
            gameObject.GetComponent<MeshRenderer>().material = baseMat;
            if(inGamePopup.inventoryShown)
            {
                inGamePopup.ShowInventory();
            }
        }
    }
}
