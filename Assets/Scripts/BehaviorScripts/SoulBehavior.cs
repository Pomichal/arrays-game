﻿using UnityEngine;

public class SoulBehavior : MonoBehaviour
{
    public int soulType;
    public bool hit = false;

    void Update()
    {
        transform.Rotate(Vector3.up * (50f * Time.deltaTime));
    }

    public void OnTriggerEnter(Collider collision)
    {
        if(!hit && collision.transform.gameObject.CompareTag("Player"))
        {
            hit = true;
            var grimReaper = collision.gameObject.GetComponent<GrimReaperBehaviour>();
            grimReaper.AddSoul(new SoulModel(GetComponent<SpriteRenderer>().sprite, soulType));
            Destroy(gameObject);
        }
    }
}
