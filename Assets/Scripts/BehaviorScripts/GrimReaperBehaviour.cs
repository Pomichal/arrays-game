﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GrimReaperBehaviour : MonoBehaviour
{
    public int score;
    public int activePortal = -1;
    public UnityEvent soulsChanged = new UnityEvent();
    public UnityEvent scoreChanged = new UnityEvent();

    public List<SoulModel> souls = new List<SoulModel>();

    public void AddSoul(SoulModel newSoulModel)
    {
        souls.Add(newSoulModel);
        soulsChanged.Invoke();
    }

    public SoulModel GetSoulModelAtIndex(int index)
    {
        return souls[index];
    }

    public void ButtonAction(int type, int index)
    {
        if(activePortal == type)
        {
            score += 10 - OperationCount(souls.Count, index);
            scoreChanged.Invoke();
            RemoveSoulAtIndex(index);
        }
    }

    public void RemoveSoulAtIndex(int index)
    {
        souls.RemoveAt(index);
        soulsChanged.Invoke();
    }

    public int OperationCount(int length, int index)
    {
        return length - (index + 1);
    }
}
