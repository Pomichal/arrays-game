﻿using UnityEngine;

public class SoulModel
{
    public Sprite sprite;
    public int type;


    public SoulModel(Sprite s, int t)
    {
        sprite = s;
        type = t;
    }
}
