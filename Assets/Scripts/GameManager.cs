﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] soulPrefabs;
    public float spawnTimerMin;
    public float spawnTimerMax;
    public Vector3 minPos;
    public bool timerOn;

    private float _timer;
    // Start is called before the first frame update
    void Start()
    {
        _timer = Random.Range(spawnTimerMin, spawnTimerMax);
        timerOn = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(timerOn)
        {
            _timer -= Time.deltaTime;
            if(_timer <= 0)
            {
                SpawnSoul();
                _timer = Random.Range(spawnTimerMin, spawnTimerMax);
            }
        }

    }

    public void SpawnSoul()
    {
        int type = Random.Range(0,3);
        var soul = Instantiate(soulPrefabs[type], new Vector3(Random.Range(minPos.x, -minPos.x),
                    2, Random.Range(minPos.z, -minPos.z)), Quaternion.identity);
    }
}
