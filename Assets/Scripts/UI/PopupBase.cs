﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupBase : MonoBehaviour
{
    public virtual void Show()
    {
    }

    public virtual void Hide()
    {
    }
}