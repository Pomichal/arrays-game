﻿using UnityEngine.UI;

public class InGamePopup : PopupBase    // actually not a popup
{
    public GrimReaperBehaviour grimReaper;
    public Text scoreText;
    public bool inventoryShown;
    public UIManager uiManager;

    public override void Show()
    {
        inventoryShown = false;
        gameObject.SetActive(true);
        grimReaper.scoreChanged.AddListener(UpdateScoreText);
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
        grimReaper.scoreChanged.RemoveListener(UpdateScoreText);
    }

    public void ShowInventory()
    {
        if(inventoryShown)
        {
            uiManager.Hide<InventoryPopup>();
        }
        else
        {
            uiManager.Show<InventoryPopup>();
        }
        inventoryShown = !inventoryShown;
    }

    void UpdateScoreText()
    {
        scoreText.text = "Score: " + grimReaper.score;
    }
}
