﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private PopupBase[] _popups;

    void Awake()
    {
        _popups = GetComponentsInChildren<PopupBase>(true);
        Show<InGamePopup>();
    }

    public void Show<T>()
    {
        foreach (var popup in _popups)
        {
            if (popup.GetType() == typeof(T))
            {
                popup.Show();
            }
        }
    }

    public void Hide <T>()
    {
        foreach (var popup in _popups)
        {
            if (popup.GetType() == typeof(T))
            {
                popup.Hide();
            }
        }
    }


}
