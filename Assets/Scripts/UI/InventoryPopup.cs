using UnityEngine;
using UnityEngine.UI;

public class InventoryPopup : PopupBase
{
    public GameObject container;
    public Button itemButton;
    public Text scoreText;
    public GrimReaperBehaviour grimReaper;

    public override void Show()
    {
        gameObject.SetActive(true);
        grimReaper.soulsChanged.AddListener(RedrawButtons);
        RedrawButtons();
    }

    public override void Hide()
    {
        gameObject.SetActive(false);
        grimReaper.soulsChanged.RemoveListener(RedrawButtons);
    }

    void RedrawButtons()
    {
        foreach (Transform child in container.transform) {
            GameObject.Destroy(child.gameObject);
        }
        for(int i=0; i < grimReaper.souls.Count; i++)
        {
            var btn = Instantiate(itemButton);
            btn.gameObject.GetComponent<Image>().sprite = grimReaper.souls[i].sprite;
            btn.gameObject.GetComponentInChildren<Text>().text = (i).ToString();
            btn.transform.SetParent(container.transform);
            int x = i;
            btn.onClick.AddListener(delegate {
                    grimReaper.ButtonAction(grimReaper.souls[x].type, x);
                    });
        }

    }
}
